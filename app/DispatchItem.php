<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DispatchItem extends Model
{
    protected $fillable = ['product_id', 'dispatch_id', 'quantity'];
}
