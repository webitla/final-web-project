<?php

namespace App;

use App\User;
use App\Supplier;
use App\DispatchItem;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

class Dispatch extends Model
{
    protected $fillable = ['user_id', 'supplier_id', 'description'];
    
    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function supplier()
    {
        return $this->hasOne(Supplier::class);
    }
    
    public function items()
    {
        return $this->hasMany(DispatchItem::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public static function validateItems(array $items)
    {
        $it = collect($items);

        //reduce: el primer valor de la iteracion devuelve null, a este se le suma el valor de la segunda
        //iteracion para devolver asi un solo valor, asi sucesivamente hasta que recorra todo el arrglo
        //al devolver null por default, puedes asignar un valor definido
        
        return $it->reduce(function($carry, $item)
        {
            $product = Product::findOrFail($item['product_id']);

            return $carry && $product->quantity;

        }, true);
    }

    public function createItems(array $items): Collection
    {
        return $this->items()->createMany($items);
    }
}

