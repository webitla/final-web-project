<?php

namespace App\Console\Commands;

use App\User;
use App\Product;
use App\Mail\restock;
use Illuminate\Http\Response;
use Illuminate\Mail\Mailable;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class RestockMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:restockMail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will send a mail with the product that are under minimun stock';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = Product::get()->where('need_stock', true);
        $user = User::find(1);
        Mail::to($user)->send(new restock($products));
        return $products->map(function ($product) {
            return $product->only(['id', 'need_stock']);
        });
    }
}
