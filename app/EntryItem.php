<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntryItem extends Model
{
    protected $fillable = [
        'product_id', 'entry_id', 'quantity'
    ];
}
