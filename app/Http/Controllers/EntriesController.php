<?php

namespace App\Http\Controllers;

use App\Entry;
use Illuminate\Http\Request;

class EntriesController extends Controller
{
    public function index(Request $request)
    {
        return Entry::with('items')->get();
    }

    public function store(Request $request)
    {
        $data = $this->validate( $request, [
            'supplier_id' => 'required',
            'user_id' => 'required',
            'description' => 'required|max:255|min:10',
            'items' => 'required|array',
            'items.*.product_id' => 'required|integer',
            'items.*.quantity' => 'required'
        ]);

        $entry = Entry::create($data);
        $entry->createItems($request->input('items'));
        $entry->save();

        return $entry;
    }

    public function show(Entry $entry)
    {
        return $entry;
    }

    public function update(Request $request, Entry $entry)
    {
        $data = $this->validate( $request, [
            'supplier_id' => 'required',
            'user_id' => 'required',
            'description' => 'required|max:255|min:10'
        ]);

        $entry->fill($data);
        $entry->save();

        return $entry;
    }

    public function destroy(Entry $entry)
    {
        $entry->delete();
    }
}