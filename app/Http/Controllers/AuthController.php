<?php

namespace App\Http\Controllers;

use JWTAuth;

use App\User;

use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Validator, DB, Hash, Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $data = $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);

        $user = User::create($data);

        // $verification_code = str_random(30); //Generate verification code
        // DB::table('user_verifications')->insert(['user_id'=>$user->id,'token'=>$verification_code]);
        // $subject = "Please verify your email address.";
        // Mail::send('email.verify', ['name' =>$user->name, 'verification_code' => $verification_code],
        //     function($mail) use ($user, $subject){
        //         $mail->from(getenv('FROM_EMAIL_ADDRESS'), "george@craftware.co");
        //         $mail->to($user->email, $user->name);
        //         $mail->subject($subject);
        //     });

            
        // return response()->json(['success'=> true, 'message'=> 'Thanks for signing up! Please check your email to complete your registration.']);
    }

        /**
     * API Verify User
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    // public function verifyUser($verification_code)
    // {
    //     $check = DB::table('user_verifications')->where('token',$verification_code)->first();
    //     if(!is_null($check)){
    //         $user = User::find($check->user_id);
    //         if($user->is_verified == 1){
    //             return response()->json([
    //                 'success'=> true,
    //                 'message'=> 'Account already verified..'
    //             ]);
    //         }
    //         $user->update(['is_verified' => 1]);
    //         DB::table('user_verifications')->where('token',$verification_code)->delete();
    //         return response()->json([
    //             'success'=> true,
    //             'message'=> 'You have successfully verified your email address.'
    //         ]);
    //     }
    //     return response()->json(['success'=> false, 'error'=> "Verification code is invalid."]);
    // }
}
