<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class MainController extends Controller
{
    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->middleware('jwt', ['except' => ['login']]);
    }
}
