<?php

namespace App\Http\Controllers;

use App\Entry;
use App\Product;
use App\Dispatch;
use Illuminate\Http\Request;

class DispatchesController extends Controller
{
    public function index()
    {
        return Dispatch::get();
    }

    public function store(Request $request)
    {
        $data = $this->validate($request,[
            'supplier_id' => 'required',
            'user_id' => 'required',
            'description' => 'required|max:255|min:10',
            'items' => 'required|array',
            'items.*.product_id' => 'required|exists:products,id',
            'items.*.quantity' => 'required'
        ]);

        abort_unless(Dispatch::validateItems($request->input('items')), 409, "No tiene suficientes productos en estock, favor revisar inventario.");

        $dispatches = Dispatch::create($data);
        $dispatches->createItems($request->input('items'));
        $dispatches->save();

        return $dispatches;

    }

    public function show(Dispatch $dispatch)
    {
        return $dispatch;
    }

    public function update(Request $request, Dispatch $dispatch)
    {
        $data = $this->validate($request,[
            'supplier_id' => 'required',
            'user_id' => 'required',
            'description' => 'required|max:255|min:10',
        ]);

        $dispatch->fill($data);
        $dispatch->save();

        return $dispatch;
    }

    public function destroy()
    {
        //
    }
}

//abort_unless(Dispatch::)
//quantutyAvailable($quantity){return $this->quantity >= $quantity}
//validateItems(array $items){$items = collect($items); return $items->reduce(function($carry, $items){ $product = Product::findOrFail(items['product_id]); return $carry && $product})}