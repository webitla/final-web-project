<?php

namespace App\Http\Controllers;

use App\Scrapping;
use Goutte\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\MainController;
use Symfony\Component\DomCrawler\Crawler;

class ScrappingController extends MainController
{
    public function gasPremium(Client $client)
    {
        $crawler = $client->request('GET', 'https://micm.gob.do/precios-de-combustibles');
        $gas = $crawler->filter("[id='art-data-table'] > tbody tr td:nth-child(2)")->first()->text();
        $fuel = floatval(preg_replace("/[^-0-9\.]/","",$gas));

        $newGas = new Scrapping();
        $newGas->precio = $fuel;
        $newGas->save();

        return $newGas;
    }
}
