<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\MainController;

class ProductsController extends MainController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Product::with('entryItems', 'dispatchItems')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $products = $this->validate($request, [
            'name' => 'required|string',
            'description' => 'required|string',
            'min_stock' => 'required|numeric',
            'measure_unit' => 'required|string',
            'price' => 'required|numeric',
            'cost' => 'required|numeric',
            'created_by' => 'required|exists:users,id',
            'updated_by' => 'required|exists:users,id',
        ]);

        return Product::create($products);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return $product;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $data = $this->validate($request, [
            'name' => 'string',
            'description' => 'string',
            'min_stock' => 'numeric',
            'measure_unit' => 'string',
            'price' => 'numeric',
            'cost' => 'numeric',
            'created_by' => 'exists:users,id',
            'updated_by' => 'exists:users,id',
        ]);

        $product->fill($data);
        $product->save();

        return $product;
    }

    public function getStock(Product $product)
    {
        return $product->getQuantityAttribute()->get();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
    }
}
