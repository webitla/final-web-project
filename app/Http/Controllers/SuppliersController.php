<?php

namespace App\Http\Controllers;

use App\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\MainController;

class SuppliersController  extends MainController
{
    public function index()
    {
        return Supplier::get();
    }

    public function store(Request $request)
    {
        $supplier = $this->validate( $request, [
            'name' => 'required|max:255',
            'direction' => 'required|max:255',
            'phone' => 'required|numeric'
        ]);

        return Supplier::create($supplier);
    }

    public function show(Supplier $supplier)
    {
        return $supplier;
    }

    public function update(Request $request, Supplier $supplier)
    {
        $data = $this->validate( $request, [
            'name' => 'max:255',
            'direction' => 'max:255',
            'phone' => 'numeric'
        ]);

        $supplier->fill($data);
        $supplier->save();

        return $supplier;
    }

    public function destroy(Supplier $supplier)
    {
        $supplier->delete();
    }
}
