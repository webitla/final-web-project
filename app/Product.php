<?php

namespace App;

use App\User;
use App\Entry;
use App\EntryItem;
use App\DispatchItem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'min_stock','cost', 'price', 'updated_by', 'created_by', 'measure_unit'
    ];

    /**
     * @return BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * @return BelongsTo
     */
    public function updater()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    //Pega esto a lo que devuelve cada que se hace el request
    protected $appends = [
        'quantity', 'need_stock'
    ];

    //Un producto tiene muchos items de entrada
    public function entryItems()
    {
        return $this->hasMany(EntryItem::class);
    }

    //Un producto tiene muchos items de salida
    public function dispatchItems()
    {
        return $this->hasMany(DispatchItem::class);
    }

    //campo que guarda la suma de los campos quantity
    public function quantity()
    {
        return $this->sum('quantity');
    }

    //Accesor que busca los campos quatity y los suma y los resta.
    public function getQuantityAttribute() //accesor
    {
        return $this->entryItems()->sum('quantity') - $this->dispatchItems()->sum('quantity');
    }

    public function getNeedStockAttribute()
    {
        $quantity = $this->quantity;
        $min_stock = $this->min_stock;
        return $quantity < $min_stock;
    }

    public function quantityAvailable($quantity)
    {
        return $this->quantity >= $quantity;
    }
}
