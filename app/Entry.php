<?php

namespace App;

use App\Product;
use App\EntryItem;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    protected $fillable = [
        'user_id', 'supplier_id', 'description'
    ];

    public function items()
    {
        return $this->hasMany(EntryItem::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function createItems(array $items): Collection
    {
        return $this->items()->createMany($items);
    }

}
