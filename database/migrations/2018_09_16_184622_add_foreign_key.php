<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('entries', function($table)
        {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('supplier_id')->references('id')->on('suppliers');
        });

        Schema::table('entry_items', function($table)
        {
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('entry_id')->references('id')->on('entries');
        });

        Schema::table('dispatches', function($table)
        {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('supplier_id')->references('id')->on('suppliers');
        });

        Schema::table('dispatch_items', function($table)
        {
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('dispatch_id')->references('id')->on('dispatches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop();
    }
}
