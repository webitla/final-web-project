<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::get('/scrapping ', function() {

//     $crawler = Goutte::request('GET', 'https://www.micm.gob.do/direcciones/hidrocarburos/avisos-semanales-de-precios/combustibles');

//     $crawler->filter('.blog-post-item ul li')->each(function ($node) {

//       dump($node->text());

//     });

// });

Route::get('/scrapping', 'ScrappingController@gasPremium');

Route::get('/stock', 'ProductsController@getStock');

//Autentication
Route::post('/auth', 'Auth\LoginController@login');
Route::delete('/auth', 'Auth\LoginController@logout');
Route::delete('/refresh', 'Auth\LoginController@refresh');
Route::post('/register', 'AuthController@register');

//Products
Route::get('/products', 'ProductsController@index');
Route::post('/products', 'ProductsController@store');
Route::get('/products/restock', 'ProductsController@restock');
Route::get('/products/{product}', 'ProductsController@show');
Route::put('/products/{product}', 'ProductsController@update');
Route::delete('/products/{product}', 'ProductsController@destroy');

//Suppliers
Route::get('/suppliers', 'SuppliersController@index');
Route::post('/suppliers', 'SuppliersController@store');
Route::get('/suppliers/{supplier}', 'SuppliersController@show');
Route::put('/suppliers/{supplier}', 'SuppliersController@update');
Route::delete('/suppliers/{supplier}', 'SuppliersController@destroy');

//Entries
Route::get('/entries', 'EntriesController@index');
Route::post('/entries', 'EntriesController@store');
Route::get('/entries/{entry}', 'EntriesController@show');
Route::put('/entries/{entry}', 'EntriesController@update');
Route::delete('/entries/{entry}', 'EntriesController@destroy');

//Dispatches
Route::get('/dispatches', 'DispatchesController@index');
Route::post('/dispatches', 'DispatchesController@store');
Route::get('/dispatches/{dispatches}', 'DispatchesController@show');
Route::put('/dispatches/{dispatches}', 'DispatchesController@update');
Route::delete('/dispatches/{dispatches}', 'DispatchesController@destroy');
