<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>

<div>
    <h1>Productos cerca de agotar existencia.</h1>

    <table>
        <thead>
            <tr>
                <th>Producto</th>
                <th>Cant. Actual</th>
                <th>Stock Minimo</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($products as $product)
                <tr>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->quantity }}</td>
                    <td>{{ $product->min_stock }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

</body>
</html>
