
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';
import App from './components/App.vue';
import Home from './components/Home.vue';
import Login from './components/Login.vue';
import Register from './components/Register.vue';

Vue.use(VueRouter)


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('login', require('./components/Login.vue'));
Vue.component('register', require('./components/Register.vue'));
Vue.component('home', require('./components/Home.vue'));
Vue.component('app', require('./components/App.vue'));

const router = new VueRouter({
    routes: [
        // dynamic segments start with a colon
      { path: '/', component: Home },
      { path: '/login', component: Login },
      { path: '/register', component: Register },
    ]
});

const app = new Vue({
    el: '#app',
    router
});
